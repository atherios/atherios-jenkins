package cms

folder('cms')
pipelineJob('cms/cms-backend-deployment') {
    logRotator {
        numToKeep(5)
    }
    definition {
        cps {
            script(readFileFromWorkspace('src/cms/CmsBackendStaging.groovy'))
        }
    }
}