package cms

folder('cms')
pipelineJob('cms/cms-frontend-deployment') {
    logRotator {
        numToKeep(5)
    }
    definition {
        cps {
            script(readFileFromWorkspace('src/cms/CmsFrontendStaging.groovy'))
        }
    }
}