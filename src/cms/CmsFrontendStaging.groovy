package cms

pipeline {
    agent any
    tools {
        'com.cloudbees.jenkins.plugins.customtools.CustomTool' 'npm-3'
        'com.cloudbees.jenkins.plugins.customtools.CustomTool' 'yarn-1'
    }
    stages {
        stage('checkout') {
            steps {
                git credentialsId: 'git', url: 'git@git.leers.pl:cms/cms-frontend.git'
            }
        }
        stage('build') {
            steps {
                sh 'yarn install'
                sh 'npm run build'
            }
        }
        stage('package & archive') {
            steps {
                sh 'zip -r dist/cms-frontend.zip dist'
                archiveArtifacts artifacts: 'dist/*.zip', fingerprint: true
            }
        }
        stage('deploy') {
            steps {
                unzip zipFile: 'dist/cms-frontend.zip', dir: '/opt/cms/cms-frontend'
                sh 'chown -R jenkins:cms /opt/cms/cms-frontend/dist'
            }
        }
    }
}