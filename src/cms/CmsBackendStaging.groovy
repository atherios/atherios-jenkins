package cms

pipeline {
    agent any
    tools {
        maven 'maven-3'
    }
    stages {
        stage('checkout') {
            steps {
                git credentialsId: 'git', url: 'git@git.leers.pl:cms/cms-backend.git'
            }
        }
        stage('build') {
            steps {
                sh 'mvn clean install -DskipTests'
            }
        }
        stage('package & archive') {
            steps {
                sh 'mvn jar:jar spring-boot:repackage'
            }
        }
        stage('deploy') {
            steps {
                sh 'sudo service cms-backend stop'
                sh 'cp target/cms-backend.jar /opt/cms/cms-backend/cms-backend.jar'
                sh 'chown -R jenkins:cms /opt/cms/cms-backend/cms-backend.jar'
                sh 'chmod 770 /opt/cms/cms-backend/cms-backend.jar'
                sh 'sudo service cms-backend start'
            }
        }
    }
}